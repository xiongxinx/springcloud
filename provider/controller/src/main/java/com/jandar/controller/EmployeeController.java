package com.jandar.controller;

import com.jandar.pojo.Employee;
import com.jandar.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class EmployeeController {
    @Autowired
    private EmployeeService employeeService;

    @GetMapping("/list")
//    @HystrixCommand(fallbackMethod = "fallback")
    public List<Employee> list() {
//        throw new RuntimeException();
        return employeeService.list();
    }

    public List<Employee>  fallback() {
        return null;
    }
}
