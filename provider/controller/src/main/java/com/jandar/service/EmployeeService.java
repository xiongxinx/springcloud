package com.jandar.service;

import com.jandar.mapper.EmployeeMapper;
import com.jandar.pojo.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {
    @Autowired
    private EmployeeMapper employeeMapper;

    public List<Employee> list() {
        return employeeMapper.list();
    }
}