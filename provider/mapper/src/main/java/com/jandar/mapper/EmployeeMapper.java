package com.jandar.mapper;

import com.jandar.pojo.Employee;

import java.util.List;

public interface EmployeeMapper {
    List<Employee> list();
}
