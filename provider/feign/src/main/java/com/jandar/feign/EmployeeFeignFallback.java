package com.jandar.feign;

import com.jandar.feign.EmployeeFeignService;
import com.jandar.pojo.Employee;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
@Component
public class EmployeeFeignFallback implements EmployeeFeignService {
    @Override
    public List<Employee> list() {
        System.out.println(12312412412L);
        return new ArrayList<>();
    }
}
