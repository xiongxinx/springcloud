package com.jandar.feign;

import com.jandar.pojo.Employee;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;
@FeignClient(value = "provider", fallback = EmployeeFeignFallback.class)
public interface EmployeeFeignService {
    @GetMapping("/list")
    List<Employee> list();
}
