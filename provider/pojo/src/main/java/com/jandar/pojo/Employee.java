package com.jandar.pojo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
public class Employee implements Serializable {
    private Long id;
    private String empName;
    private String empGender;
    private String empEmail;
    private Long dId;
}
