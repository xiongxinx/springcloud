package com.jandar.util;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author 12467
 */
@Configuration
public class MyRule {
    @Bean
    public IRule getIRule() {
        return new RandomRule();
    }
}
