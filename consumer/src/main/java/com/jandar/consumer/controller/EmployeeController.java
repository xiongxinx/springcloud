package com.jandar.consumer.controller;

import com.jandar.feign.EmployeeFeignService;
import com.jandar.pojo.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@RestController
public class EmployeeController {
    @Autowired
//    private RestTemplate restTemplate;
    private EmployeeFeignService employeeFeignService;
    @GetMapping("list")
    public List<Employee> list () {
//        return restTemplate.getForObject("http://provider/list",List.class);
        return employeeFeignService.list();
    }
}
